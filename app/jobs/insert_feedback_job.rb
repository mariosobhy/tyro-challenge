class InsertFeedbackJob < ApplicationJob
  queue_as :default
  include DataTypesConversion

  def perform(feedback,state)
    # Do something later
    feedback = Feedback.create! json_parse(feedback)
    puts state
    state = State.new json_parse(state) 
    feedback.state = state
  end
end
