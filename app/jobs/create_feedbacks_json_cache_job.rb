class CreateFeedbacksJsonCacheJob < ApplicationJob
  queue_as :default

  def perform(company_token)
    # Do something later
    @feedbacks = $redis.get(company_token)
    if @feedbacks.nil? 
      @feedbacks = Feedback.company(company_token).to_json
      $redis.set(company_token,@feedbacks)
      # Expire the cache, every 3 hours
      $redis.expire(company_token,3.hours)
    else 
      $redis.del "#{company_token}" 
      @feedbacks = Feedback.company(company_token).to_json
      $redis.set(company_token,@feedbacks)
    end   
  end
end
