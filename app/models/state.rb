require 'elasticsearch/model'

class State < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  
  #-------------------------------- Associations -----------------------------------
  belongs_to :feedback

  #-------------------------------- Enums ------------------------------------------
  enum os: [:android,:ios,:windows_mobile]
end

