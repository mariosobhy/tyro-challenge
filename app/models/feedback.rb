class Feedback < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  #index_name Rails.application.class.parent_name.underscore
  #document_type self.name.downcase
  settings do
    mapping dynamic: false do
      indexes :company_token,type: :text, analyzer: :english
      indexes :number,type: :integer
      indexes :state do 
        indexes :device
        indexes :os
      end 
    end
  end

  #-------------------------------- Associations -----------------------------------
  has_one :state, dependent: :destroy
  has_secure_token :company_token
  accepts_nested_attributes_for :state
  
  #-------------------------------- Enums ------------------------------------------
  enum priority: [:minor,:major,:critical]
  
  #-------------------------------- Validations ------------------------------------
  validates_uniqueness_of :number, scope: :company_token

  #-------------------------------- Callbacks --------------------------------------
  after_save    { Indexer.perform_async(:index,  self.id) }
  after_destroy { Indexer.perform_async(:delete, self.id) }

  #-------------------------------- Scopes --------------------------------------
  scope :company, ->(company_token) { where company_token: company_token }

  def self.find_feedback(company_token,number)
      self.search({
        sort:[ 
          number: { "unmapped_type": "long" }
        ],
      query: {
              bool: {
                must: [
                  { term: { company_token: company_token } },
                  { term: { number: number } }
                ]
              }
      }
    })
  end 

  def self.custom_search(query)
      self.search({
        sort:[ 
          number: { 'unmapped_type': "long" }
        ],
      query: {
        bool: {
          must: [
            { term: { company_token: query } }
          ]
        }
      }
    })
  end 

  def as_indexed_json(options={})
    self.as_json(
      include: {state: {only: [:device,:os,:memory,:storage]}}
    )
  end

  def perform_later_insertion(number) 
    self.number = number
    puts self.id
    InsertFeedbackJob.perform_later(self)
  end 

=begin 
  private
  def check_company_token_existence
    feedback = Feedback.where(company_token: self.company_token).last
    if !feedback.nil? 
      self.number = feedback.number + 1
    else 
      self.number = 1
    end 
  end 

  def clear_cache
    $redis.del "#{self.company_token}"
  end 

  def create_json_cache 
    CreateFeedbacksJsonCacheJob.perform_later(self.company_token)
  end 
=end
end

