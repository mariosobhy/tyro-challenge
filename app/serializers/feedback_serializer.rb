class FeedbackSerializer < ActiveModel::Serializer
  attributes *Feedback.column_names
  has_one :state
end
