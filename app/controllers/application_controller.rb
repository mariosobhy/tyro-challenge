class ApplicationController < ActionController::API
  def fetch_feedbacks(company_token)
    @feedbacks = $redis.get(company_token)
    if @feedbacks.nil? 
      nil
      # Expire the cache, every 3 hours
      #$redis.expire(params[:company_token])
    end   
    @feedbacks
  end 
end
