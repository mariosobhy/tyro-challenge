class FeedbacksController < ApplicationController
  include DataTypesConversion
  include Response
  before_action :set_feedback, only: [:show]

  # GET /feedbacks
  def index
    # by redis 
    #@feedbacks = fetch_feedbacks(params[:company_token])

    # by Elasticsearch 
    @feedbacks = Feedback.custom_search(params[:company_token])
    if @feedbacks.count == 0 
      json_response(error: "Not found",status: 404)
    else 
      json_response(@feedbacks)
    end 
  end

  # GET /feedbacks/1
  def show
    if @feedback.count == 0
      json_response(error: "Not found",status: 404)
    else   
      json_response(@feedback)
    end
  end

  def count 
    @feedbacks = Feedback.custom_search(params[:company_token])
    if @feedbacks.count == 0 
       json_response(error: "Not found",status: 404)
    else 
       json_response(count: @feedbacks.count)
    end 
  end 

  # POST /feedbacks
  def create
    @feedback = Feedback.new(feedback_params)
    @feedbacks = Feedback.custom_search(@feedback.company_token)
    if @feedbacks.count == 0
      @feedback.number = 1
      json_response(number: @feedback.number, status: :created)
    else 
      #return number from cache 
      @feedback.number = @feedbacks.count + 1
      json_response(number: @feedback.number, status: :created)
    end 
    #handle insertion in a background job
    InsertFeedbackJob.perform_later(@feedback.to_json,@feedback.state.to_json)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_feedback
      @feedback = Feedback.find_feedback(params[:company_token],params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def feedback_params
      params.require(:feedback).permit(:company_token, :number, :priority,state_attributes: [:id,:device,:os,:memory,:storage])
    end
end
