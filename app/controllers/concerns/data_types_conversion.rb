# all shared methods between controllers
module DataTypesConversion
  extend ActiveSupport::Concern
  
  def json_parse(object)
    JSON.parse(object)
  end

  def convert_to_hash(object)
    hash = object.instance_variables.each_with_object({}) { |var, hash| hash[var.to_s.delete("@")] = object.instance_variable_get(var) }
  end
end

