# README

# Project Title

feedback system with an company token

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* [ElasticSearch](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-elasticsearch-on-ubuntu-16-04) - Search Engine Framework
* [MySql](https://support.rackspace.com/how-to/installing-mysql-server-on-ubuntu/) - MySql database

### Installing

- Install ruby 2.3+
- Run `bundle install` to get the required dependencies
- Run `bundle exec rake db:create`, `bundle exec rake db:migrate`, and `bundle exec rake db:seed` to set up the database
- Run `sudo /etc/init.d/elasticsearch start` to start elasticsearch
- Run `rails c`
- type `Feedback.import force: true` and excute it 
- type `State.import force: true` and excute it
- Then test the api with any tool you love (ex: postman)


## Running the tests

- Run the test suite using `rspec`

### And coding style tests

- Run rubocop using `bundle exec rubocop` to check code style

## Built With

* [Ruby On Rails](https://rubyonrails.org/) - The web framework used
* [ElasticSearch](https://github.com/elastic/elasticsearch-rails) - Search Engine Framework
* [Sidkiq](https://github.com/mperham/sidekiq) - Background processing for Ruby
* [Redis](https://github.com/redis-store/redis-rails) - in-memory data structure
* [Rspec](https://github.com/rspec/rspec) - Testing framework
