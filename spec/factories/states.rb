FactoryBot.define do
  factory :state do
    device "MyString"
    os 1
    memory 1
    storage 1
    feedback nil
  end
end
