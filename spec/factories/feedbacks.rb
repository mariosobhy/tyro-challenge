FactoryBot.define do
  factory :feedback do
    company_token "MyString"
    number 1
    priority 1
  end
end
